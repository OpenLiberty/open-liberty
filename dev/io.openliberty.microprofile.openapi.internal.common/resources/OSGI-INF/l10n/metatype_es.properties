###############################################################################
# Copyright (c) 2023 IBM Corporation and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
###############################################################################
# -------------------------------------------------------------------------------------------------
#CMVCPATHNAME io.openliberty.microprofile.openapi.internal.common/resources/OSGI-INF/l10n/metatype.properties
#ISMESSAGEFILE FALSE
#NLS_ENCODING=UNICODE
#NLS_MESSAGEFORMAT_NONE

mpOpenAPI=MicroProfile OpenAPI
mpOpenAPI.desc=Configuraci\u00f3n para MicroProfile OpenAPI.

docPath=V\u00eda de acceso de punto final de documento de MicroProfile OpenAPI
docPath.desc=Especifica la v\u00eda de acceso de URL para obtener documentos de OpenAPI . La v\u00eda de acceso de URL debe construirse con caracteres alfanum\u00e9ricos Unicode A-Za-z0-9, subrayado (_), gui\u00f3n (-), barra inclinada (/) o punto (.).

uiPath=V\u00eda de acceso de punto final de interfaz de usuario de MicroProfile OpenAPI
uiPath.desc=Especifica la v\u00eda de acceso de URL para acceder a la interfaz de usuario de MicroProfile OpenAPI . La v\u00eda de acceso de URL debe construirse con caracteres alfanum\u00e9ricos Unicode A-Za-z0-9, subrayado (_), gui\u00f3n (-), barra inclinada (/) o punto (.) Si no se especifica ning\u00fan valor, la v\u00eda de acceso se establece en $docPath/ui.

includeApplication=Aplicaciones incluidas en MicroProfile OpenAPI 
includeApplication.desc=Nombres de aplicaciones, uno por elemento, que deben incluirse en el documento OpenAPI. El nombre de una aplicaci\u00f3n puede definirse en el server.xml; de lo contrario, se utilizar\u00e1 por defecto el nombre del archivo de la aplicaci\u00f3n sin ninguna extensi\u00f3n.  El valor especial "todas" incluye todas las aplicaciones disponibles. 

excludeApplication=Aplicaciones MicroProfile OpenAPI excluidas
excludeApplication.desc=Nombres de aplicaciones, uno por elemento, que deben excluirse del documento OpenAPI.

includeModule=M\u00f3dulos incluidos en MicroProfile OpenAPI 
includeModule.desc=Nombres de m\u00f3dulos, uno por elemento, que deben incluirse en el documento OpenAPI. Los nombres de los m\u00f3dulos deben tener el formato {ModuleName}. El nombre de un m\u00f3dulo puede definirse en su descriptor de despliegue; de lo contrario, se utilizar\u00e1 por defecto el nombre de archivo del m\u00f3dulo, excluyendo cualquier extensi\u00f3n. Tambi\u00e9n se registra en el mensaje SRVE0169I.

excludeModule=M\u00f3dulos MicroProfile OpenAPI excluidos
excludeModule.desc=Nombres de m\u00f3dulos, uno por elemento, que deben excluirse del documento OpenAPI.

openAPIVersion=Versi\u00f3n de especificaci\u00f3n de OpenAPI
openAPIVersion.desc=La versi\u00f3n de la especificaci\u00f3n OpenAPI que debe utilizarse para el documento OpenAPI. mpOpenAPI-4.0 por defecto es3.1" y tambi\u00e9n soporta3.0". Las versiones anteriores s\u00f3lo admiten3.0".

info=Informaci\u00f3n sobre el documento OpenAPI 
info.desc=Establece la secci\u00f3n de informaci\u00f3n del documento OpenAPI, anulando cualquier secci\u00f3n de informaci\u00f3n proporcionada en la aplicaci\u00f3n.

title=T\u00edtulo de API
title.desc=Establece info.title en el documento OpenAPI. Este campo contiene el t\u00edtulo de la API que se est\u00e1 documentando.

description=Descripci\u00f3n de la API
description.desc=Establece info.description en el documento OpenAPI. Este campo contiene una descripci\u00f3n de la API que se est\u00e1 documentando.

termsOfService=URL de las condiciones de servicio de la API
termsOfService.desc=Establece el campo info.termsOfService en el documento OpenAPI. Este campo contiene la URL del documento de condiciones de servicio de la API que se est\u00e1 documentando.

contactName=Nombre del contacto API
contactName.desc=Establece el campo info.contact.name en el documento OpenAPI. Este campo contiene el nombre de un contacto responsable de la API que se est\u00e1 documentando.

contactUrl=URL de contacto de la API
contactUrl.desc=Establece el campo info.contact.url en el documento OpenAPI. Este campo contiene la URL de un contacto responsable de la API que se est\u00e1 documentando.

contactEmail=Correo electr\u00f3nico de contacto de la API
contactEmail.desc=Establece el campo info.contact.email en el documento OpenAPI. Este campo contiene la direcci\u00f3n de correo electr\u00f3nico de un contacto responsable de la API que se est\u00e1 documentando.

licenseName=Nombre de la licencia API
licenseName.desc=Establece el campo info.license.name en el documento OpenAPI. Este campo contiene el nombre de la licencia de la API que se est\u00e1 documentando.

licenseUrl=URL de licencia API
licenseUrl.desc=Establece el campo info.license.url en el documento OpenAPI. Este campo contiene una URL en la que se puede encontrar la licencia completa de la API que se est\u00e1 documentando.

licenseIdentifier=Identificador de licencia API
licenseIdentifier.desc=Establece el campo info.license.identifier en el documento OpenAPI. Este campo contiene una expresi\u00f3n de licencia SPDX para la licencia de la API que se est\u00e1 documentando. Este atributo se ignora si la versi\u00f3n de OpenAPI es inferior a 3.1.

summary=Resumen de la API
summary.desc=Establece el campo info.summary en el documento OpenAPI. Este campo contiene un breve resumen de la API que se est\u00e1 documentando. Este atributo se ignora si la versi\u00f3n de OpenAPI es inferior a 3.1.

version=N\u00famero de versi\u00f3n de la API
version.desc=Establece el campo info.version en el documento OpenAPI. Este campo contiene el n\u00famero de versi\u00f3n de la API que se est\u00e1 documentando.
