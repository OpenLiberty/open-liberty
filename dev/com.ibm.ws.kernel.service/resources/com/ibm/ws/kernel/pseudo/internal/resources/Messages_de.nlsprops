###############################################################################
# Copyright (c) 2011, 2025 IBM Corporation and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
###############################################################################
# # {0} description of each insert field
# MSG_DESCRIPTIVE_NAME_CWSJX0000=CWSJX0000I: This is a message with inserts {0}
# MSG_DESCRIPTIVE_NAME_CWSJX0000.explanation=Explanation text for the message
# MSG_DESCRIPTIVE_NAME_CWSJX0000.useraction=User action text for the message
#
#CMVCPATHNAME com.ibm.ws.kernel.service/resources/com/ibm/ws/kernel/pseudo/internal/resources/Messages.nlsprops
#COMPONENTPREFIX CWWKE
#COMPONENTNAMEFOR CWWKE WebSphere kernel bootstrap, command line, and location service
#ISMESSAGEFILE TRUE
#NLS_ENCODING=UNICODE
#
# NLS_MESSAGEFORMAT_VAR
#
#   Strings in this file which contain replacement variables are processed by the MessageFormat 
#   class (single quote must be coded as 2 consecutive single quotes ''). Strings in this file 
#   which do NOT contain replacement variables are NOT processed by the MessageFormat class 
#   (single quote must be coded as one single quote '). 
# -------------------------------------------------------------------------------------------------
# 0800 - 0899 com.ibm.ws.kernel.pseudo.internal.PseudoServiceMessages
# -------------------------------------------------------------------------------------------------

jndi.not.installed=CWWKE0800W: Es wurde versucht, einen Anfangskontext f\u00fcr [{0}] abzurufen, aber es ist kein JNDI-Feature konfiguriert.
jndi.not.installed.explanation=Das JNDI-Feature stellt eine JNDI-Registry und zugeh\u00f6rige Dienstprogramme bereit. Dies ist gew\u00f6hnlich erforderlich, wenn eine Anwendung oder ein Feature Eintr\u00e4ge in einem JNDI-Kontext suchen muss.
jndi.not.installed.useraction=Stellen Sie sicher, dass eine JNDI-Implementierung bereitgestellt wird (z. B. durch Konfiguration des JNDI-Features in der Konfiguration f\u00fcr diesen Server).

# # {0} description of each insert field
# MSG_DESCRIPTIVE_NAME_CWWKS0000=CWWKS0000I: This is a message with inserts {0}
# MSG_DESCRIPTIVE_NAME_CWWKS0000.explanation=Explanation text for the message
# MSG_DESCRIPTIVE_NAME_CWWKS0000.useraction=User action text for the message
#
#CMVCPATHNAME com.ibm.ws.common.crypto
#COMPONENTPREFIX CWWKS
#COMPONENTNAMEFOR WebSphere Application Server Security Common Crypto Utilities
#ISMESSAGEFILE TRUE
#NLS_MESSAGEFORMAT_VAR
#NLS_ENCODING=UNICODE

# -------------------------------------------------------------------------------------------------
# CWWKS5900 - CWWKS5910 com.ibm.ws.common.crypto
# -------------------------------------------------------------------------------------------------
#Message prefix block: CWWKS5900 - CWWKS5910
CRYPTO_INSECURE=CWWKS5900W: Das Konfigurationselement {0} gibt den unsicheren kryptografischen Algorithmus {1} an.  Verwenden Sie stattdessen den sicheren Algorithmus {2}.
CRYPTO_INSECURE.explanation=Der konfigurierte Algorithmus gilt nicht mehr als kryptografisch sicher.
CRYPTO_INSECURE.useraction=Verwenden Sie den vorgeschlagenen sicheren Algorithmus.

CRYPTO_INSECURE_REPLACED=CWWKS5901W: Das Konfigurationselement {0} gibt den unsicheren kryptografischen Algorithmus {1} an.  Stattdessen wird der sichere Algorithmus {2} verwendet.
CRYPTO_INSECURE_REPLACED.explanation=Der unsichere kryptografische Algorithmus wurde durch den sicheren Algorithmus ersetzt.
CRYPTO_INSECURE_REPLACED.useraction=Es ist keine Aktion erforderlich.

CRYPTO_INSECURE_PROVIDER=CWWKS5902W: Das Konfigurationselement {0} verwendet den unsicheren kryptografischen Algorithmus {1}. Erw\u00e4gen Sie, die Konfiguration durch einen anderen Anbieter zu ersetzen, wenn die Sicherheit ein Problem darstellt.
CRYPTO_INSECURE_PROVIDER.explanation=Der Algorithmus gilt nicht mehr als kryptografisch sicher.
CRYPTO_INSECURE_PROVIDER.useraction=Erw\u00e4gen Sie, die Konfiguration durch einen anderen Anbieter zu ersetzen, wenn die Sicherheit ein Problem darstellt.

FIPS_140_3ENABLED=CWWKS5903I: FIPS 140-3 ist aktiviert und verwendet den FIPS-Anbieter {0}
FIPS_140_3ENABLED.explanation=FIPS 140-3 ist aktiviert.
FIPS_140_3ENABLED.useraction=Diese Nachricht ist nur f\u00fcr Informationszwecke bestimmt. Es ist keine Aktion erforderlich.

FIPS_140_3ENABLED_ERROR=CWWKS5904E: Die FIPS 140-3-Eigenschaften sind konfiguriert, aber der FIPS-Anbieter ist nicht verf\u00fcgbar.
FIPS_140_3ENABLED_ERROR.explanation=FIPS 140-3 kann nicht aktiviert werden, da der FIPS-Anbieter nicht verf\u00fcgbar ist. Stellen Sie sicher, dass das Betriebssystem FIPS unterst\u00fctzt, die Java-Version mit FIPS 140-3 kompatibel ist und der FIPS-Anbieter in der Datei java.security als h\u00f6chste Priorit\u00e4t konfiguriert ist.
FIPS_140_3ENABLED_ERROR.useraction=Verwenden Sie eine FIPS 140-3-konforme Java-Version und Betriebssystemplattform.
