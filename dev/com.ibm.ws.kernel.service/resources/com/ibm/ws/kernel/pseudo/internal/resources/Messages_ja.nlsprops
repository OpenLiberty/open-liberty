###############################################################################
# Copyright (c) 2011, 2025 IBM Corporation and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
###############################################################################
# # {0} description of each insert field
# MSG_DESCRIPTIVE_NAME_CWSJX0000=CWSJX0000I: This is a message with inserts {0}
# MSG_DESCRIPTIVE_NAME_CWSJX0000.explanation=Explanation text for the message
# MSG_DESCRIPTIVE_NAME_CWSJX0000.useraction=User action text for the message
#
#CMVCPATHNAME com.ibm.ws.kernel.service/resources/com/ibm/ws/kernel/pseudo/internal/resources/Messages.nlsprops
#COMPONENTPREFIX CWWKE
#COMPONENTNAMEFOR CWWKE WebSphere kernel bootstrap, command line, and location service
#ISMESSAGEFILE TRUE
#NLS_ENCODING=UNICODE
#
# NLS_MESSAGEFORMAT_VAR
#
#   Strings in this file which contain replacement variables are processed by the MessageFormat 
#   class (single quote must be coded as 2 consecutive single quotes ''). Strings in this file 
#   which do NOT contain replacement variables are NOT processed by the MessageFormat class 
#   (single quote must be coded as one single quote '). 
# -------------------------------------------------------------------------------------------------
# 0800 - 0899 com.ibm.ws.kernel.pseudo.internal.PseudoServiceMessages
# -------------------------------------------------------------------------------------------------

jndi.not.installed=CWWKE0800W: [{0}] \u306e\u521d\u671f\u30b3\u30f3\u30c6\u30ad\u30b9\u30c8\u3092\u691c\u7d22\u3057\u3088\u3046\u3068\u3057\u307e\u3057\u305f\u304c\u3001\u69cb\u6210\u3055\u308c\u305f JNDI \u30d5\u30a3\u30fc\u30c1\u30e3\u30fc\u306f\u3042\u308a\u307e\u305b\u3093\u3002
jndi.not.installed.explanation=JNDI \u30d5\u30a3\u30fc\u30c1\u30e3\u30fc\u306f\u3001JNDI \u30ec\u30b8\u30b9\u30c8\u30ea\u30fc\u3068\u3001\u95a2\u9023\u3059\u308b\u30e6\u30fc\u30c6\u30a3\u30ea\u30c6\u30a3\u30fc\u3092\u63d0\u4f9b\u3057\u307e\u3059\u3002  \u3053\u308c\u306f\u3001\u30a2\u30d7\u30ea\u30b1\u30fc\u30b7\u30e7\u30f3\u307e\u305f\u306f\u30d5\u30a3\u30fc\u30c1\u30e3\u30fc\u304c JNDI \u30b3\u30f3\u30c6\u30ad\u30b9\u30c8\u306e\u4e2d\u3067\u30a8\u30f3\u30c8\u30ea\u30fc\u3092\u30eb\u30c3\u30af\u30a2\u30c3\u30d7\u3059\u308b\u5fc5\u8981\u304c\u3042\u308b\u5834\u5408\u3001\u901a\u5e38\u5fc5\u8981\u3067\u3059\u3002
jndi.not.installed.useraction=JNDI \u5b9f\u88c5\u304c\u63d0\u4f9b\u3055\u308c\u3066\u3044\u308b\u3053\u3068 (\u4f8b\u3048\u3070\u3001\u3053\u306e\u30b5\u30fc\u30d0\u30fc\u306e\u69cb\u6210\u306e\u4e2d\u306b JNDI \u30d5\u30a3\u30fc\u30c1\u30e3\u30fc\u3092\u69cb\u6210\u3059\u308b\u3053\u3068\u306b\u3088\u308a) \u3092\u78ba\u8a8d\u3057\u3066\u304f\u3060\u3055\u3044\u3002

# # {0} description of each insert field
# MSG_DESCRIPTIVE_NAME_CWWKS0000=CWWKS0000I: This is a message with inserts {0}
# MSG_DESCRIPTIVE_NAME_CWWKS0000.explanation=Explanation text for the message
# MSG_DESCRIPTIVE_NAME_CWWKS0000.useraction=User action text for the message
#
#CMVCPATHNAME com.ibm.ws.common.crypto
#COMPONENTPREFIX CWWKS
#COMPONENTNAMEFOR WebSphere Application Server Security Common Crypto Utilities
#ISMESSAGEFILE TRUE
#NLS_MESSAGEFORMAT_VAR
#NLS_ENCODING=UNICODE

# -------------------------------------------------------------------------------------------------
# CWWKS5900 - CWWKS5910 com.ibm.ws.common.crypto
# -------------------------------------------------------------------------------------------------
#Message prefix block: CWWKS5900 - CWWKS5910
CRYPTO_INSECURE=CWWKS5900W: {0} \u69cb\u6210\u8981\u7d20\u306f\u3001 {1} \u306e\u5b89\u5168\u3067\u306a\u3044\u6697\u53f7\u5316\u30a2\u30eb\u30b4\u30ea\u30ba\u30e0\u3092\u6307\u5b9a\u3057\u307e\u3059\u3002  \u4ee3\u308f\u308a\u306b {2} \u306e\u5b89\u5168\u306a\u30a2\u30eb\u30b4\u30ea\u30ba\u30e0\u3092\u4f7f\u7528\u3059\u308b\u3053\u3068\u3092\u691c\u8a0e\u3057\u3066\u304f\u3060\u3055\u3044\u3002
CRYPTO_INSECURE.explanation=\u8a2d\u5b9a\u3055\u308c\u305f\u30a2\u30eb\u30b4\u30ea\u30ba\u30e0\u306f\u3001\u3082\u306f\u3084\u6697\u53f7\u7684\u306b\u5b89\u5168\u3068\u306f\u307f\u306a\u3055\u308c\u307e\u305b\u3093\u3002
CRYPTO_INSECURE.useraction=\u63a8\u5968\u3055\u308c\u305f\u5b89\u5168\u306a\u30a2\u30eb\u30b4\u30ea\u30ba\u30e0\u3092\u4f7f\u7528\u3057\u3066\u304f\u3060\u3055\u3044\u3002

CRYPTO_INSECURE_REPLACED=CWWKS5901W: {0} \u69cb\u6210\u8981\u7d20\u306f\u3001 {1} \u306e\u5b89\u5168\u3067\u306a\u3044\u6697\u53f7\u5316\u30a2\u30eb\u30b4\u30ea\u30ba\u30e0\u3092\u6307\u5b9a\u3057\u307e\u3059\u3002  \u4ee3\u308f\u308a\u306b {2} \u306e\u30bb\u30ad\u30e5\u30a2\u306a\u30a2\u30eb\u30b4\u30ea\u30ba\u30e0\u304c\u4f7f\u7528\u3055\u308c\u307e\u3059\u3002
CRYPTO_INSECURE_REPLACED.explanation=\u5b89\u5168\u6027\u306e\u4f4e\u3044\u6697\u53f7\u5316\u30a2\u30eb\u30b4\u30ea\u30ba\u30e0\u306f\u3001\u5b89\u5168\u6027\u306e\u9ad8\u3044\u30a2\u30eb\u30b4\u30ea\u30ba\u30e0\u306b\u7f6e\u304d\u63db\u3048\u3089\u308c\u307e\u3057\u305f\u3002
CRYPTO_INSECURE_REPLACED.useraction=\u30a2\u30af\u30b7\u30e7\u30f3\u306f\u4e0d\u8981\u3067\u3059\u3002

CRYPTO_INSECURE_PROVIDER=CWWKS5902W: {0} \u69cb\u6210\u8981\u7d20\u306f\u3001 {1} \u3068\u3044\u3046\u5b89\u5168\u3067\u306a\u3044\u6697\u53f7\u5316\u30a2\u30eb\u30b4\u30ea\u30ba\u30e0\u3092\u4f7f\u7528\u3057\u3066\u3044\u307e\u3059\u3002 \u30bb\u30ad\u30e5\u30ea\u30c6\u30a3\u304c\u61f8\u5ff5\u4e8b\u9805\u3067\u3042\u308b\u5834\u5408\u306f\u3001\u5225\u306e\u30d7\u30ed\u30d0\u30a4\u30c0\u30fc\u306b\u69cb\u6210\u3092\u5909\u66f4\u3059\u308b\u3053\u3068\u3092\u691c\u8a0e\u3057\u3066\u304f\u3060\u3055\u3044\u3002
CRYPTO_INSECURE_PROVIDER.explanation=\u3053\u306e\u30a2\u30eb\u30b4\u30ea\u30ba\u30e0\u306f\u3001\u3082\u306f\u3084\u6697\u53f7\u7684\u306b\u5b89\u5168\u3068\u306f\u307f\u306a\u3055\u308c\u307e\u305b\u3093\u3002
CRYPTO_INSECURE_PROVIDER.useraction=\u30bb\u30ad\u30e5\u30ea\u30c6\u30a3\u304c\u61f8\u5ff5\u4e8b\u9805\u3067\u3042\u308b\u5834\u5408\u306f\u3001\u5225\u306e\u30d7\u30ed\u30d0\u30a4\u30c0\u30fc\u306b\u69cb\u6210\u3092\u5909\u66f4\u3059\u308b\u3053\u3068\u3092\u691c\u8a0e\u3057\u3066\u304f\u3060\u3055\u3044\u3002

FIPS_140_3ENABLED=CWWKS5903I: FIPS 140-3 \u304c\u6709\u52b9\u306b\u306a\u3063\u3066\u304a\u308a\u3001FIPS \u30d7\u30ed\u30d0\u30a4\u30c0\u30fc\u3092\u4f7f\u7528\u3057\u3066\u3044\u308b {0}
FIPS_140_3ENABLED.explanation=FIPS 140-3 \u304c\u6709\u52b9\u306b\u306a\u3063\u3066\u3044\u307e\u3059\u3002
FIPS_140_3ENABLED.useraction=\u3053\u306e\u30e1\u30c3\u30bb\u30fc\u30b8\u306f\u60c5\u5831\u63d0\u4f9b\u306e\u307f\u3092\u76ee\u7684\u3068\u3057\u3066\u3044\u307e\u3059\u3002 \u30a2\u30af\u30b7\u30e7\u30f3\u306f\u4e0d\u8981\u3067\u3059\u3002

FIPS_140_3ENABLED_ERROR=CWWKS5904E: FIPS 140-3\u306e\u30d7\u30ed\u30d1\u30c6\u30a3\u306f\u8a2d\u5b9a\u3055\u308c\u3066\u3044\u308b\u304c\u3001FIPS\u30d7\u30ed\u30d0\u30a4\u30c0\u306f\u5229\u7528\u3067\u304d\u306a\u3044\u3002
FIPS_140_3ENABLED_ERROR.explanation=FIPS\u30d7\u30ed\u30d0\u30a4\u30c0\u304c\u5229\u7528\u3067\u304d\u306a\u3044\u305f\u3081\u3001FIPS 140-3\u3092\u6709\u52b9\u306b\u3067\u304d\u307e\u305b\u3093\u3002 \u30aa\u30da\u30ec\u30fc\u30c6\u30a3\u30f3\u30b0\u30b7\u30b9\u30c6\u30e0\u304cFIPS\u3092\u30b5\u30dd\u30fc\u30c8\u3057\u3001Java\u306e\u30d0\u30fc\u30b8\u30e7\u30f3\u304cFIPS 140-3\u3068\u4e92\u63db\u6027\u304c\u3042\u308a\u3001FIPS\u30d7\u30ed\u30d0\u30a4\u30c0\u30fc\u304c java.security \u30d5\u30a1\u30a4\u30eb\u5185\u3067\u6700\u512a\u5148\u3068\u3057\u3066\u8a2d\u5b9a\u3055\u308c\u3066\u3044\u308b\u3053\u3068\u3092\u78ba\u8a8d\u3057\u3066\u304f\u3060\u3055\u3044\u3002
FIPS_140_3ENABLED_ERROR.useraction=FIPS 140-3\u306b\u6e96\u62e0\u3057\u305fJava\u30d0\u30fc\u30b8\u30e7\u30f3\u3068OS\u30d7\u30e9\u30c3\u30c8\u30d5\u30a9\u30fc\u30e0\u3092\u4f7f\u7528\u3057\u3066\u304f\u3060\u3055\u3044\u3002
