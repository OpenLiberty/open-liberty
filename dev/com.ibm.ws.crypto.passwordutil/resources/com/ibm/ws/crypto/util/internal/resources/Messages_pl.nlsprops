###############################################################################
# Copyright (c) 2016, 2025 IBM Corporation and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     IBM Corporation - initial API and implementation
###############################################################################
#CMVCPATHNAME com.ibm.ws.crypto.passwordutil/resources/com/ibm/ws/crypto/util/internal/resources/Messages.nlsprops
#COMPONENTPREFIX CWWKS
#COMPONENTNAMEFOR WebSphere Application Security passwordUtil
#ISMESSAGEFILE TRUE
#NLS_ENCODING=UNICODE
# NLS_MESSAGEFORMAT_VAR
#

# Message prefix block: CWWKS1850 - CWWKS1899

PASSWORDUTIL_CUSTOM_SERVICE_STARTED=CWWKS1850I: Uruchomiono niestandardow\u0105 us\u0142ug\u0119 szyfrowania has\u0142a. Nazwa klasy: {0}.
PASSWORDUTIL_CUSTOM_SERVICE_STARTED.explanation=Ten komunikat ma charakter wy\u0142\u0105cznie informacyjny.
PASSWORDUTIL_CUSTOM_SERVICE_STARTED.useraction=Nie jest wymagana \u017cadna czynno\u015b\u0107.
PASSWORDUTIL_CUSTOM_SERVICE_STOPPED=CWWKS1851I: Zatrzymano niestandardow\u0105 us\u0142ug\u0119 szyfrowania has\u0142a. Nazwa klasy: {0}.
PASSWORDUTIL_CUSTOM_SERVICE_STOPPED.explanation=Ten komunikat ma charakter wy\u0142\u0105cznie informacyjny.
PASSWORDUTIL_CUSTOM_SERVICE_STOPPED.useraction=Nie jest wymagana \u017cadna czynno\u015b\u0107.
PASSWORDUTIL_CUSTOM_DECRYPTION_ERROR=CWWKS1852E: Wyst\u0105pi\u0142 nieoczekiwany wyj\u0105tek podczas deszyfrowania has\u0142a przy u\u017cyciu niestandardowej us\u0142ugi szyfrowania has\u0142a.
PASSWORDUTIL_CUSTOM_DECRYPTION_ERROR.explanation=Wyst\u0105pi\u0142 nieoczekiwany wyj\u0105tek podczas przetwarzania deszyfrowania przy u\u017cyciu niestandardowej us\u0142ugi szyfrowania has\u0142a.
PASSWORDUTIL_CUSTOM_DECRYPTION_ERROR.useraction=Sprawd\u017a dzienniki niestandardowej us\u0142ugi szyfrowania has\u0142a i dzienniki serwera, aby uzyska\u0107 wi\u0119cej informacji na temat przyczyny wyst\u0105pienia tego wyj\u0105tku.
PASSWORDUTIL_CUSTOM_ENCRYPTION_ERROR=CWWKS1853E: Wyst\u0105pi\u0142 nieoczekiwany wyj\u0105tek podczas szyfrowania has\u0142a przy u\u017cyciu niestandardowej us\u0142ugi szyfrowania has\u0142a.
PASSWORDUTIL_CUSTOM_ENCRYPTION_ERROR.explanation=Wyst\u0105pi\u0142 nieoczekiwany wyj\u0105tek podczas przetwarzania szyfrowania przy u\u017cyciu niestandardowej us\u0142ugi szyfrowania has\u0142a.
PASSWORDUTIL_CUSTOM_ENCRYPTION_ERROR.useraction=Sprawd\u017a dzienniki niestandardowej us\u0142ugi szyfrowania has\u0142a i dzienniki serwera, aby uzyska\u0107 wi\u0119cej informacji na temat przyczyny wyst\u0105pienia tego wyj\u0105tku.
PASSWORDUTIL_CUSTOM_SERVICE_DOES_NOT_EXIST=CWWKS1854E: Niestandardowa us\u0142uga hase\u0142 innej firmy nie jest dost\u0119pna w celu przetworzenia niestandardowo zakodowanego has\u0142a.
PASSWORDUTIL_CUSTOM_SERVICE_DOES_NOT_EXIST.explanation=Nie mo\u017cna przetworzy\u0107 niestandardowo zakodowanego has\u0142a, poniewa\u017c niestandardowa us\u0142uga hase\u0142 jest niedost\u0119pna.
PASSWORDUTIL_CUSTOM_SERVICE_DOES_NOT_EXIST.useraction=Sprawd\u017a, czy skonfigurowano i uruchomiono funkcj\u0119 hase\u0142 niestandardowych u\u017cytkownika, kt\u00f3ra implementuje interfejs com.ibm.wsspi.security.crypto.CustomPasswordEncryption.
PASSWORDUTIL_UNKNOWN_ALGORITHM=CWWKS1855E: Nie przetworzono has\u0142a, poniewa\u017c nazwa algorytmu has\u0142a {0} nie jest obs\u0142ugiwana. Obs\u0142ugiwane s\u0105 typy: {1}.
PASSWORDUTIL_UNKNOWN_ALGORITHM.explanation=Nie zaszyfrowano ani nie zdeszyfrowano has\u0142a, poniewa\u017c podana nazwa algorytmu has\u0142a nie jest obs\u0142ugiwana.
PASSWORDUTIL_UNKNOWN_ALGORITHM.useraction=Nale\u017cy u\u017cy\u0107 jednego z obs\u0142ugiwanych algorytm\u00f3w.
PASSWORDUTIL_UNKNOWN_ALGORITHM_EXCEPTION=CWWKS1856E: Nie przetworzono has\u0142a, poniewa\u017c zg\u0142oszono wyj\u0105tek nieznanego algorytmu has\u0142a.
PASSWORDUTIL_UNKNOWN_ALGORITHM_EXCEPTION.explanation=Okre\u015blony algorytm nie jest obs\u0142ugiwany.
PASSWORDUTIL_UNKNOWN_ALGORITHM_EXCEPTION.useraction=Sprawd\u017a dziennik serwera. Je\u015bli u\u017cywane jest niestandardowe szyfrowanie has\u0142a, sprawd\u017a dodatkowe informacje w dzienniku niestandardowego szyfrowania has\u0142a.
PASSWORDUTIL_CYPHER_EXCEPTION=CWWKS1857E: Nie przetworzono has\u0142a, poniewa\u017c zg\u0142oszono wyj\u0105tek niepoprawnego szyfru has\u0142a.
PASSWORDUTIL_CYPHER_EXCEPTION.explanation=Nie zaszyfrowano ani nie zdeszyfrowano has\u0142a, poniewa\u017c zg\u0142oszono wyj\u0105tek podczas przetwarzania has\u0142a.
PASSWORDUTIL_CYPHER_EXCEPTION.useraction=Sprawd\u017a dziennik serwera. Je\u015bli u\u017cywane jest niestandardowe szyfrowanie has\u0142a, sprawd\u017a dodatkowe informacje w dzienniku niestandardowego szyfrowania has\u0142a.
PASSWORDUTIL_UNSUPPORTEDENCODING_EXCEPTION=CWWKS1858E: Nie przetworzono has\u0142a, poniewa\u017c zg\u0142oszono wyj\u0105tek nieobs\u0142ugiwanego kodowania.
PASSWORDUTIL_UNSUPPORTEDENCODING_EXCEPTION.explanation=Nie zaszyfrowano ani nie zdeszyfrowano has\u0142a, poniewa\u017c zg\u0142oszono wyj\u0105tek podczas przetwarzania has\u0142a.
PASSWORDUTIL_UNSUPPORTEDENCODING_EXCEPTION.useraction=Sprawd\u017a dziennik serwera. Je\u015bli u\u017cywane jest niestandardowe szyfrowanie has\u0142a, sprawd\u017a dodatkowe informacje w dzienniku niestandardowego szyfrowania has\u0142a.
PASSWORDUTIL_INVALID_BASE64_STRING=CWWKS1859E: Nie zdeszyfrowano has\u0142a, poniewa\u017c zg\u0142oszono b\u0142\u0105d dekodowania.
PASSWORDUTIL_INVALID_BASE64_STRING.explanation=Zaszyfrowane has\u0142o nie zosta\u0142o poprawnie zakodowane.
PASSWORDUTIL_INVALID_BASE64_STRING.useraction=Upewnij si\u0119, \u017ce zaszyfrowane has\u0142o nie jest obci\u0119te i zakodowane za pomoc\u0105 kodowania Base64.
PASSWORDUTIL_EXCEPTION_FIPS140_3_AES128_UNAVAILABLE_ALGORITHM=CWWKS1860E: AES-128 zaszyfrowane has\u0142a nie mog\u0105 by\u0107 u\u017cywane, gdy FIPS 140-3 jest w\u0142\u0105czony. Zregeneruj has\u0142o za pomoc\u0105 AES-256, u\u017cywaj\u0105c polecenia securityUtility.
PASSWORDUTIL_EXCEPTION_FIPS140_3_AES128_UNAVAILABLE_ALGORITHM.explanation=FIPS 140-3 jest w\u0142\u0105czony i nie obs\u0142uguje AES-128. U\u017cyj polecenia securityUtility, aby zaszyfrowa\u0107 has\u0142o za pomoc\u0105 AES-256.
PASSWORDUTIL_EXCEPTION_FIPS140_3_AES128_UNAVAILABLE_ALGORITHM.useraction=Upewnij si\u0119, \u017ce has\u0142o jest zaszyfrowane za pomoc\u0105 AES-256.
PASSWORDUTIL_UNAVAILABLE_ENCRYPTION_ALGORITHM_EXCEPTION=CWWKS1861E: Szyfrowanie has\u0142a AES nie powiod\u0142o si\u0119. Sprawd\u017a ustawienia java.security i dodaj brakuj\u0105cy algorytm.
PASSWORDUTIL_UNAVAILABLE_ENCRYPTION_ALGORITHM_EXCEPTION.explanation=W pliku java.security brakuje algorytmu szyfrowania.
PASSWORDUTIL_UNAVAILABLE_ENCRYPTION_ALGORITHM_EXCEPTION.useraction=Upewnij si\u0119, \u017ce algorytm zosta\u0142 dodany do pliku java.security.
PASSWORDUTIL_UNAVAILABLE_DECRYPTION_ALGORITHM_EXCEPTION=CWWKS1862E: Odszyfrowanie has\u0142a AES nie powiod\u0142o si\u0119. Sprawd\u017a ustawienia java.security i dodaj brakuj\u0105cy algorytm.
PASSWORDUTIL_UNAVAILABLE_DECRYPTION_ALGORITHM_EXCEPTION.explanation=W pliku java.security brakuje algorytmu szyfrowania.
PASSWORDUTIL_UNAVAILABLE_DECRYPTION_ALGORITHM_EXCEPTION.useraction=Upewnij si\u0119, \u017ce algorytm zosta\u0142 dodany do pliku java.security.
PASSWORDUTIL_EXCEPTION_FIPS140_3_HASH_SHA1_UNAVAILABLE_ALGORITHM=CWWKS1863E: Nie mo\u017cna u\u017cywa\u0107 hash-kodowanych hase\u0142 SHA1, gdy w\u0142\u0105czony jest standard FIPS 140-3. Wygeneruj ponownie skr\u00f3t has\u0142a za pomoc\u0105 polecenia securityUtility.
PASSWORDUTIL_EXCEPTION_FIPS140_3_HASH_SHA1_UNAVAILABLE_ALGORITHM.explanation=FIPS 140-3 jest w\u0142\u0105czony i nie obs\u0142uguje SHA1. U\u017cyj polecenia securityUtility encode, aby wygenerowa\u0107 nowy skr\u00f3t has\u0142a SHA512.
PASSWORDUTIL_EXCEPTION_FIPS140_3_HASH_SHA1_UNAVAILABLE_ALGORITHM.useraction=Upewnij si\u0119, \u017ce hash has\u0142a zosta\u0142 wygenerowany za pomoc\u0105 SHA512.
PASSWORDUTIL_WEAK_ALGORITHM_WARNING=CWWKS1864W: Co najmniej jedna warto\u015b\u0107 has\u0142a {0} przechowywana w konfiguracji serwera zosta\u0142a wygenerowana przy u\u017cyciu starszego algorytmu {1}. U\u017cyj polecenia securityUtility encode, aby ponownie wygenerowa\u0107 has\u0142a {0} przy u\u017cyciu najnowszego algorytmu {2}.
PASSWORDUTIL_WEAK_ALGORITHM_WARNING.explanation=Liberty u\u017cywa teraz silniejszych algorytm\u00f3w dla nowo generowanych hase\u0142 {aes} i {hash}.
PASSWORDUTIL_WEAK_ALGORITHM_WARNING.useraction=Upewnij si\u0119, \u017ce warto\u015bci hase\u0142 {aes} i {hash} zosta\u0142y wygenerowane przy u\u017cyciu najnowszej wersji Liberty.


# ---------------- This is an exception message ------------
PASSWORDUTIL_DUPLICATE_CUSTOM_ENCRYPTION=Wykryto wi\u0119cej ni\u017c jedn\u0105 implementacj\u0119 CustomPasswordEncryption. Obs\u0142ugiwana jest tylko jedna implementacja CustomPasswordEncryption. Lista wykrytych implementacji CustomPasswordEncryption jest nast\u0119puj\u0105ca:
PASSWORDUTIL_ERROR_IN_EXTENSION_MANIFEST_FILE=Zg\u0142oszono b\u0142\u0105d podczas przetwarzania pliku manifestu rozszerzenia {0}. Ten plik zosta\u0142 zignorowany. Szyfrowanie niestandardowe nie jest dost\u0119pne. Komunikat o b\u0142\u0119dzie: {1}.
PASSWORDUTIL_ERROR_MISSING_HEADER=Nie znaleziono wymaganego nag\u0142\u00f3wka {0} w pliku manifestu rozszerzenia {1}. Szyfrowanie niestandardowe nie jest dost\u0119pne.
PASSWORDUTIL_ERROR_NO_FEATURE_MANIFEST=Nie znaleziono pliku manifestu sk\u0142adnika, kt\u00f3ry odwo\u0142uje si\u0119 do pliku manifestu rozszerzenia {0}. Szyfrowanie niestandardowe nie jest dost\u0119pne.
PASSWORDUTIL_ERROR_UNSUPPORTED_OPERATION=Operacja deszyfrowania nie jest obs\u0142ugiwana przez algorytm hash.
