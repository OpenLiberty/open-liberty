#CMVCPATHNAME N/A
#COMPONENTPREFIX CWMOT
#COMPONENTNAMEFOR CWMOT MicroProfile Telemetry Tracing
#NLS_ENCODING=UNICODE
#NLS_MESSAGEFORMAT_NONE
#ISMESSAGEFILE true
# -------------------------------------------------------------------------------------------------
#*******************************************************************************
# Copyright (c) 2023, 2024 IBM Corporation and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-2.0/
# 
# SPDX-License-Identifier: EPL-2.0
# 
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
# This file follows the WebSphere Message Guidelines.
# For more information, visit:
# http://washome.austin.ibm.com/xwiki/bin/view/MessagesTeam/
# 
#-----------------------------------------------------------------------------------------------------------------------------
# Message prefix block: CWMOT5000 - CWMOT5999
#-----------------------------------------------------------------------------------------------------------------------------
# Message prefix block for io.openliberty.microprofile.telemetry.common: CWMOT5000 - CWMOT5199
# This was previously two files, so the message IDs used are not contiguous but should not be changed.

CWMOT5100.tracing.is.disabled=CWMOT5100I: Funkcja MicroProfile Telemetry Tracing est w\u0142\u0105czona, ale nie zosta\u0142a skonfigurowana w celu generowania danych \u015bledzenia dla aplikacji {0}.
CWMOT5100.tracing.is.disabled.explanation=Aby mo\u017cna by\u0142o utworzy\u0107 \u015bledzenie i wyeksportowa\u0107 je do systemu \u015bledzenia, nale\u017cy skonfigurowa\u0107 funkcj\u0119 MicroProfile Telemetry Tracing.
CWMOT5100.tracing.is.disabled.useraction=Aby w\u0142\u0105czy\u0107 \u015bledzenie, nale\u017cy poda\u0107 warto\u015b\u0107 otel.sdk.disabled=false w dowolnym ze \u017ar\u00f3de\u0142 konfiguracji dost\u0119pnych za pomoc\u0105 funkcji MicroProfile Config. Mo\u017ce by\u0107 wymagana dalsza konfiguracja, aby umo\u017cliwi\u0107 funkcji MicroProfile Telemetry Tracing wyeksportowanie danych \u015bledzenia do systemu \u015bledzenia.

CWMOT5000.cannot.get.globalopentelemetry=CWMOT5000W: Wywo\u0142ano metod\u0119 GlobalOpenTelemetry.get. Ta metoda zwraca niefunkcjonalny obiekt OpenTelemetry. Zamiast tego nale\u017cy u\u017cy\u0107 funkcji CDI, aby wstrzykn\u0105\u0107 obiekt OpenTelemetry.
CWMOT5000.cannot.get.globalopentelemetry.explanation=Klasa GlobalOpenTelemetry nie jest obs\u0142ugiwana, poniewa\u017c nie istnieje \u017caden globalny obiekt OpenTelemetry. Zamiast tego ka\u017cda aplikacja musi u\u017cy\u0107 funkcji CDI, aby wstrzykn\u0105\u0107 obiekt OpenTelemetry dla tej aplikacji.
CWMOT5000.cannot.get.globalopentelemetry.useraction=U\u017cyj funkcji CDI, aby wstrzykn\u0105\u0107 obiekt OpenTelemetry.

CWMOT5001.cannot.set.globalopentelemetry=CWMOT5001E: Ustawianie obiektu GlobalOpenTelemetry nie jest obs\u0142ugiwane.
CWMOT5001.cannot.set.globalopentelemetry.explanation=Klasa GlobalOpenTelemetry nie jest obs\u0142ugiwana, poniewa\u017c nie istnieje \u017caden globalny obiekt OpenTelemetry. Zamiast tego ka\u017cda aplikacja musi u\u017cy\u0107 funkcji CDI, aby wstrzykn\u0105\u0107 obiekt OpenTelemetry dla tej aplikacji.
CWMOT5001.cannot.set.globalopentelemetry.useraction=Nie wywo\u0142uj metody GlobalOpenTelemetry.get. Gdy kod aplikacji wymaga uzyskania obiektu OpenTelemetry, u\u017cyj funkcji CDI do przeprowadzenia wstrzykni\u0119cia.

CWMOT5002.telemetry.error=CWMOT5002E: W sk\u0142adniku MicroProfile Telemetry wyst\u0105pi\u0142 b\u0142\u0105d wewn\u0119trzny. B\u0142\u0105d: {0}.
CWMOT5002.telemetry.error.explanation=Wyst\u0105pi\u0142 b\u0142\u0105d, kt\u00f3ry uniemo\u017cliwi\u0142 normalne dzia\u0142anie sk\u0142adnika MicroProfile Telemetry.
CWMOT5002.telemetry.error.useraction=Przejrzyj plik dziennika serwera message.log oraz dzienniki FFDC, aby zidentyfikowa\u0107 problem.

CWMOT5003.factory.used.after.shutdown=CWMOT5003W: Aplikacja {0} podj\u0119\u0142a pr\u00f3b\u0119 uzyskania sk\u0142adnika MicroProfile Telemetry po zamkni\u0119ciu aplikacji.
CWMOT5003.factory.used.after.shutdown.explanation=CWMOT5003.factory.used.after.shutdown.explanation= Po zamkni\u0119ciu aplikacji sk\u0142adnik MicroProfile Telemetry zwraca obiekty, kt\u00f3re w trybie cichym wykonuj\u0105 dzia\u0142ania bez operacji po ich wywo\u0142aniu. Ten proces mo\u017ce prowadzi\u0107 do braku oczekiwanych danych telemetrycznych.
CWMOT5003.factory.used.after.shutdown.useraction=Przejrzyj aplikacj\u0119, aby sprawdzi\u0107, dlaczego pr\u00f3bowa\u0142a ona u\u017cy\u0107 sk\u0142adnika MicroProfile Telemetry po zamkni\u0119ciu. Dzia\u0142ania, kt\u00f3re mog\u0105 wyzwoli\u0107 u\u017cycie sk\u0142adnika MicroProfile Telemetry, obejmuj\u0105 wywo\u0142anie metody z adnotacj\u0105 @WithSpan lub wykonanie \u017c\u0105dania za pomoc\u0105 klienta JAX-RS lub klienta MP Rest.

CWMOT5004.factory.used.without.metadata=CWMOT5004W: Z\u0142o\u017cono wniosek o OpenTelemetry obiekt, ale bez metadanych aplikacji i bez danych globalnych OpenTelemetry obiekt by\u0142 dost\u0119pny. 
CWMOT5004.factory.used.without.metadata.explanation=CWMOT5004.factory.used.without.metadata.explanation=If nie ma globala OpenTelemetry obiekt Liberty'S MicroProfile Kod integracji telemetrii podejmie pr\u00f3b\u0119 znalezienia prawid\u0142owego OpenTelemetry obiekt dla bie\u017c\u0105cej aplikacji przy u\u017cyciu metadanych aplikacji w bie\u017c\u0105cym w\u0105tku. Poniewa\u017c jednak w w\u0105tku nie by\u0142o metadanych, nie mo\u017cna by\u0142o tego zrobi\u0107. 
CWMOT5004.factory.used.without.metadata.useraction=Przejrzyj sw\u00f3j kod, aby sprawdzi\u0107, dlaczego kod integracyjny pr\u00f3bowa\u0142 u\u017cy\u0107 MicroProfile Telemetry bez metadanych i obiektu OpenTelemetry o zakresie serwerowym. Kod integracji najprawdopodobniej pr\u00f3buje utworzy\u0107 nowe w\u0105tki bez korzystania z interfejsu ManagedExecutorService i pozyska\u0107 obiekt OpenTelemetry z poziomu aplikacji. Aby uzyska\u0107 obiekt OpenTemetry poza aplikacj\u0105, nale\u017cy w\u0142\u0105czy\u0107 OpenTelemetry za pomoc\u0105 zmiennej \u015brodowiskowej lub w\u0142a\u015bciwo\u015bci systemowej.

CWMOT5005.mptelemetry.unknown.log.source=CWMOT5005W: Funkcja rejestrowania telemetrii MicroProfile nie rozpoznaje \u017ar\u00f3d\u0142a rejestrowania [ {0} ].
CWMOT5005.mptelemetry.unknown.log.source.explanation=\u015arodowisko wykonawcze Liberty nie udost\u0119pnia okre\u015blonego \u017ar\u00f3d\u0142a dziennika.
CWMOT5005.mptelemetry.unknown.log.source.useraction=Sprawd\u017a informacje o konfiguracji i okre\u015bl jedno z obs\u0142ugiwanych \u017ar\u00f3de\u0142 dla telemetrii MicroProfile .

CWMOT5006.tel.enabled.conflict=CWMOT5006W: Wykryto konflikt konfiguracji w\u0142a\u015bciwo\u015bci otel.sdk.disabled dla aplikacji {0} . Warto\u015b\u0107 ko\u0144cowa to otel.sdk.disabled =false. Telemetrii nie mo\u017cna wy\u0142\u0105czy\u0107 dla aplikacji, je\u015bli jest ona w\u0142\u0105czona w \u015brodowisku wykonawczym.
CWMOT5006.tel.enabled.conflict.explanation: Telemetri\u0119 MicroProfile mo\u017cna w\u0142\u0105czy\u0107 dla ca\u0142ego serwera za pomoc\u0105 w\u0142a\u015bciwo\u015bci systemu lub zmiennych \u015brodowiskowych. Je\u017celi opcja ta nie jest w\u0142\u0105czona dla ca\u0142ego serwera, mo\u017cna j\u0105 w\u0142\u0105czy\u0107 dla ka\u017cdej aplikacji, ustawiaj\u0105c w\u0142a\u015bciwo\u015bci konfiguracji MicroProfile , na przyk\u0142ad ustawiaj\u0105c zmienne w pliku server.xml . To ostrze\u017cenie jest wy\u015bwietlane, je\u015bli istnieje wyra\u017ana konfiguracja wy\u0142\u0105czaj\u0105ca telemetri\u0119 dla ca\u0142ego serwera, kt\u00f3ra jest zast\u0119powana przez w\u0142a\u015bciwo\u015b\u0107 MP Config w\u0142\u0105czaj\u0105c\u0105 telemetri\u0119 dla aplikacji.
CWMOT5006.tel.enabled.conflict.useraction: Okre\u015bl ustawienia w\u0142\u0105czania lub wy\u0142\u0105czania wyst\u0105pie\u0144 OpenTelemetry za pomoc\u0105 zmiennych \u015brodowiskowych i w\u0142a\u015bciwo\u015bci systemu lub innych \u017ar\u00f3de\u0142 MP Config, ale nie obu naraz.

CWMOT5007.tel.enabled.conflict=CWMOT5007W: Wykryto konflikt konfiguracji w\u0142a\u015bciwo\u015bci otel.sdk.disabled dla aplikacji {0} . Warto\u015b\u0107 ko\u0144cowa to otel.sdk.disabled =false, poniewa\u017c w\u0142a\u015bciwo\u015b\u0107 w\u0142\u0105czaj\u0105ca telemetri\u0119 dla aplikacji zast\u0119puje w\u0142a\u015bciwo\u015b\u0107 wy\u0142\u0105czaj\u0105c\u0105 telemetri\u0119 dla \u015brodowiska wykonawczego.
CWMOT5007.tel.enabled.conflict.explanation: Telemetri\u0119 MicroProfile mo\u017cna w\u0142\u0105czy\u0107 dla ca\u0142ego serwera za pomoc\u0105 w\u0142a\u015bciwo\u015bci systemu lub zmiennych \u015brodowiskowych. Je\u017celi opcja ta nie jest w\u0142\u0105czona dla ca\u0142ego serwera, mo\u017cna j\u0105 w\u0142\u0105czy\u0107 dla aplikacji, ustawiaj\u0105c w\u0142a\u015bciwo\u015bci konfiguracji MicroProfile , na przyk\u0142ad ustawiaj\u0105c zmienne w pliku server.xml . To ostrze\u017cenie jest wy\u015bwietlane, gdy telemetria jest w\u0142\u0105czona dla ca\u0142ego serwera, ale w\u0142a\u015bciwo\u015bci konfiguracji MicroProfile wy\u0142\u0105czaj\u0105 telemetri\u0119 dla aplikacji. Poniewa\u017c telemetria jest w\u0142\u0105czona dla ca\u0142ego serwera, konfiguracja dla tej aplikacji jest ignorowana.
CWMOT5007.tel.enabled.conflict.useraction: Okre\u015bl ustawienia w\u0142\u0105czania lub wy\u0142\u0105czania wyst\u0105pie\u0144 OpenTelemetry za pomoc\u0105 zmiennych \u015brodowiskowych i w\u0142a\u015bciwo\u015bci systemu lub innych \u017ar\u00f3de\u0142 MP Config, ale nie obu naraz.


#-----------------------------------------------------------------------------------------------------------------------------
# Monitor Metrics messages
#-----------------------------------------------------------------------------------------------------------------------------

threadpool.activeThreads.description=Liczba w\u0105tk\u00f3w, w kt\u00f3rych aktywnie wykonywane s\u0105 zadania.
threadpool.size.description=Wielko\u015b\u0107 puli w\u0105tk\u00f3w.

session.activeSessions.description=Liczba jednocze\u015bnie aktywnych sesji. Sesja jest aktywna, gdy \u015brodowisko wykonawcze przetwarza \u017c\u0105danie korzystaj\u0105ce z danej sesji u\u017cytkownika.
session.created.description=Liczba sesji zalogowanych od momentu w\u0142\u0105czenia tej metryki.
session.invalidated.description=Liczba sesji wylogowanych od momentu w\u0142\u0105czenia tej metryki.
session.invalidatedbyTimeout.description=Liczba sesji, kt\u00f3re zosta\u0142y wylogowane z powodu przekroczenia limitu czasu od momentu w\u0142\u0105czenia tej metryki.
session.live.description=Liczba u\u017cytkownik\u00f3w, kt\u00f3rzy s\u0105 obecnie zalogowani.

connectionpool.handle.count.description=Liczba po\u0142\u0105cze\u0144, kt\u00f3re s\u0105 w u\u017cyciu. Liczba ta mo\u017ce obejmowa\u0107 wiele po\u0142\u0105cze\u0144 wsp\u00f3\u0142u\u017cytkowanych z jednego po\u0142\u0105czenia zarz\u0105dzanego.
connectionpool.connection.created.description=\u0141\u0105czna liczba zarz\u0105dzanych po\u0142\u0105cze\u0144, kt\u00f3re utworzono od momentu utworzenia puli.
connectionpool.connection.destroyed.description=\u0141\u0105czna liczba zarz\u0105dzanych po\u0142\u0105cze\u0144, kt\u00f3re zosta\u0142y zniszczone od momentu utworzenia puli.
connectionpool.connection.free.description=Liczba dost\u0119pnych zarz\u0105dzanych po\u0142\u0105cze\u0144.
connectionpool.connection.waitTime.description=Ca\u0142kowity czas oczekiwania \u017c\u0105da\u0144 po\u0142\u0105czenia na po\u0142\u0105czenie.
connectionpool.connection.useTime.description=Ca\u0142kowity czas, w kt\u00f3rym wszystkie po\u0142\u0105czenia ze \u017ar\u00f3d\u0142em danych by\u0142y w u\u017cyciu.
connectionpool.connection.count.description=Aktualna liczba zarz\u0105dzanych po\u0142\u0105cze\u0144 w puli. Warto\u015b\u0107 ta obejmuje zar\u00f3wno dost\u0119pne, jak i u\u017cywane po\u0142\u0105czenia zarz\u0105dzane. Pojedyncze zarz\u0105dzane po\u0142\u0105czenie wsp\u00f3\u0142dzielone przez wiele po\u0142\u0105cze\u0144 liczy si\u0119 tylko raz.

requestTiming.processed.description=Liczba \u017c\u0105da\u0144 serwlet\u00f3w od uruchomienia serwera.
requestTiming.active.description=Liczba \u017c\u0105da\u0144 serwletu, kt\u00f3re s\u0105 aktualnie uruchomione.
requestTiming.slow.description=Liczba obecnie realizowanych, ale wolno dzia\u0142aj\u0105cych \u017c\u0105da\u0144 serwlet\u00f3w.
requestTiming.hung.description=Liczba \u017c\u0105da\u0144 serwletu, kt\u00f3re s\u0105 aktualnie zawieszone.

#-----------------------------------------------------------------------------------------------------------------------------
# HTTP Metrics messages
#-----------------------------------------------------------------------------------------------------------------------------

http.server.request.duration.description=Czas trwania \u017c\u0105da\u0144 serwera HTTP.
